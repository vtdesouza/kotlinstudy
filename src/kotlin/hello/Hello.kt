package kotlin.hello

fun main(args: Array<String>) {
//    hello(args)
//    testJoin(args)
//    displaySeparator(size = 3)
//    displaySeparator()
//    displaySeparator(character = '#')
//    displaySeparator('8', 10)
//    forLoop()
//    ranges()
//    javaTypes()
//    loopWithIndex()
    nullableType("S1", null)
}

fun testJoin (args: Array<String>) {
    println(listOf('a', 'b', 'c').joinToString(separator = "", prefix = "(", postfix = ")"))
    println(listOf(1, 2, 3).joinToString(postfix = "."))
}

fun hello(args: Array<String>) =
//    val name = if (args.isNotEmpty()) args[0] else "Kotlin"
//    println("Hello, $name!")
    println("Hello, ${(if (args.isNotEmpty()) args[0] else "Kotlin!")}")

fun displaySeparator(character: Char = '*', size: Int = 10) {
    repeat(size) {
        print(character)
    }
    println()
}

fun forLoop() {
    println("=== Ex 01:")
    var list = listOf("a", "b", "c")
    for ((index, element) in list.withIndex()) {
        println("$index: $element")
    }
    println()

    println("=== Ex 02:")
    for (i in 1..9) {
        print(i)
    }
    println()

    println("=== Ex 03:")
    for (i in 1 until 10) {
        print(i)
    }
    println()

    println("=== Ex 04:")
    for (i in 9 downTo 1) {
        print(i)
    }
    println()

    println("=== Ex 05:")
    for (i in 9 downTo 1 step 2) {
        print(i)
    }
    println()

    println("=== Ex 06:")
    for (i in 1..9 step 2) {
        print(i)
    }
    println()

    println("=== Ex 07:")
    for (ch in "abc") {
        print(ch + 1)
    }
    println()
}

fun ranges() {
    println("isLetter(q) = ${isLetter('q')}")
    println("isLetter(*) = ${isLetter('*')}")
    println("isNotDigit(A) = ${isNotDigit('A')}")
    println("isNotDigit(7) = ${isNotDigit('7')}")
    println("recognize('5') = ${recognize('5')}")
    println("recognize('D') = ${recognize('D')}")
    println("recognize('*') = ${recognize('*')}")
}

fun isLetter(c:Char) = c in 'a' .. 'z' || c in 'A' .. 'Z'

fun isNotDigit(c:Char) = c !in '0'..'9'

fun recognize(c:Char) = when (c) {
    in '0'..'9' -> "Digit"
    in 'a' .. 'z', in 'A' .. 'Z' -> "Letter"
    else -> "???"
}

fun javaTypes() {
    println(hashSetOf(1,2,3).javaClass)
    println(arrayListOf(1,2,3).javaClass)
    println(listOf(1,2,3).javaClass)
    println(hashMapOf(1 to "one", 2 to "two", 3 to "three").javaClass)
}

/**
 * Sumário da documentação
 *
 * Teste de documentação de métodos/funções no Kotlin
 */
fun loopWithIndex() {
    val list = listOf("A", "B", "C")
    for ((index, element) in list.withIndex()) {
        println("$index : $element")
    }
}

fun nullableType(s1:String, s2:String?) {

    val l1: Int = s1.length
    val l2: Int? = s2?.length ?: 7

    println("l1: ${l1.javaClass}\ts1: ${s1.javaClass}")
    println("l2: ${l2?.javaClass}\ts2: ${s2?.javaClass}")

}