package board

import board.Direction.*
import java.lang.IllegalArgumentException

fun createSquareBoard(width: Int): SquareBoard = SquareBoardImpl(width)
fun <T> createGameBoard(width: Int): GameBoard<T> = GameBoardImpl<T>(width)

open class SquareBoardImpl(override val width: Int): SquareBoard {
    protected open val board: List<List<Cell>>

    init {
        val mBoard: Array<Array<Cell?>> = Array<Array<Cell?>>(width) { arrayOfNulls<Cell>(width) }

        val coords = (1..width).flatMap { i -> (1..width).map { j -> i to j }}

        coords.forEach {
            val cell = Cell(it.first, it.second)
            mBoard[cell.i-1][cell.j-1] = cell
        }

        board = mBoard.mapNotNull { b -> b.mapNotNull { it } }
    }

    override fun getCellOrNull(i: Int, j: Int): Cell? {
        return board.getOrNull(i-1)?.getOrNull(j-1)
    }

    override fun getCell(i: Int, j: Int): Cell {
        return getCellOrNull(i, j) ?: throw IllegalArgumentException()
    }

    override fun getAllCells(): Collection<Cell> {
        return board.flatten()
    }

    override fun getRow(i: Int, jRange: IntProgression): List<Cell> {
        val l = board[i-1].filter { it.j in jRange }
        return if (jRange.first <= jRange.last) l else l.reversed()
    }

    override fun getColumn(iRange: IntProgression, j: Int): List<Cell> {
        val c = getAllCells().filter { it.i in iRange && it.j == j}
        return if (iRange.first <= iRange.last)  c else c.reversed()
    }

    override fun Cell.getNeighbour(direction: Direction): Cell? {
        return when (direction) {
            UP -> getCellOrNull(this.i-1, this.j)
            LEFT -> getCellOrNull(this.i, this.j-1)
            RIGHT -> getCellOrNull(this.i, this.j+1)
            DOWN -> getCellOrNull(this.i+1, this.j)
        }
    }

}

class GameBoardImpl<T>(width: Int): SquareBoardImpl(width), GameBoard<T> {

    private val boardMap = HashMap(getAllCells().map { Pair<Cell, T?>(it, null) }.toMap())

    override fun get(cell: Cell): T? = boardMap[cell]

    override fun set(cell: Cell, value: T?) {
        boardMap += cell to value
    }

    override fun filter(predicate: (T?) -> Boolean): Collection<Cell> = boardMap.filter { predicate.invoke(it.value) }.map { it.key }

    override fun find(predicate: (T?) -> Boolean): Cell? = filter { predicate.invoke(it) }.first()

    override fun any(predicate: (T?) -> Boolean): Boolean = boardMap.any { predicate.invoke(it.value) }

    override fun all(predicate: (T?) -> Boolean): Boolean = boardMap.all { predicate.invoke(it.value) }
}