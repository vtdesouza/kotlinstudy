package games.gameOfFifteen

import board.Cell
import board.Direction
import board.GameBoard
import board.createGameBoard
import games.game.Game

/*
 * Implement the Game of Fifteen (https://en.wikipedia.org/wiki/15_puzzle).
 * When you finish, you can play the game by executing 'PlayGameOfFifteen'.
 */
fun newGameOfFifteen(initializer: GameOfFifteenInitializer = RandomGameInitializer()): Game =
        GameOfFifteen(initializer)


class GameOfFifteen(private val initializer: GameOfFifteenInitializer) : Game {

    val board = createGameBoard<Int?>(4)

    override fun initialize() {
        val permutation = initializer.initialPermutation.toMutableList<Int?>()
        permutation.add(null)
        board.getAllCells().forEachIndexed { index, cell -> board[cell] = permutation[index] }
    }

    override fun canMove(): Boolean = true

    override fun hasWon(): Boolean {
        with(board) {
            var base = -1
            getAllCells().map { get(it) ?: 100 }.forEach { if (base > it) return false else base = it }
            return true
        }
    }

    override fun processMove(direction: Direction) {
        val nullCell = board.find { it == null }
        var neighbour: Cell?
        board.apply { neighbour = nullCell!!.getNeighbour(direction.reversed()) }
        if (neighbour != null) {
            board[nullCell!!] = board[neighbour!!]
            board[neighbour!!] = null
        }
    }

    override fun get(i: Int, j: Int): Int? {
        return board[board.getCell(i, j)]
    }

}

fun main() {
    val game = GameOfFifteen(initializer = RandomGameInitializer())
    game.initialize()
}

fun <T> GameBoard<T>.printBoard() {
    for (i in 1..width) {
        println(getRow(i, 1..width))
    }
}

fun <T> GameBoard<T>.printBoardValues() {
    for (i in 1..width) {
        println(getRow(i, 1..width).map { get(it) })
    }
}