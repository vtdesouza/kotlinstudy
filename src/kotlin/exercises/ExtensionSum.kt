package kotlin.exercises

fun sum(list: List<Int>): Int {
    var result = 0
    for (i in list) {
        result += i
    }
    return result
}

fun List<Int>.sum2(): Int {
    var result = 0
    for (i in this) {
        result += i
    }
    return result
}

fun main(args: Array<String>) {
    val list = listOf(1, 2, 3)
    val sum = sum(list)
    println(sum)    // 6
    println(list.sum2())
}