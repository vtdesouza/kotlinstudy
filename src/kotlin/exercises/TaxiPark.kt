package kotlin.exercises


data class TaxiPark(
    val allDrivers: Set<Driver>,
    val allPassengers: Set<Passenger>,
    val trips: List<Trip>)

data class Driver(val name: String)
data class Passenger(val name: String)

data class Trip(
    val driver: Driver,
    val passengers: Set<Passenger>,
    // the trip duration in minutes
    val duration: Int,
    // the trip distance in km
    val distance: Double,
    // the percentage of discount (in 0.0..1.0 if not null)
    val discount: Double? = null
) {
    // the total cost of the trip
    val cost: Double
        get() = (1 - (discount ?: 0.0)) * (duration + distance)
}

/*
 * Task #1. Find all the drivers who performed no trips.
 */
fun TaxiPark.findFakeDrivers(): Set<Driver> =
    allDrivers.filterNot { d -> d in trips.map { it.driver }.distinct() }.toSet()

/*
 * Task #2. Find all the clients who completed at least the given number of trips.
 */
fun TaxiPark.findFaithfulPassengers(minTrips: Int): Set<Passenger> =
    allPassengers.filter { p -> trips.flatMap { it.passengers }.count { p == it } >= minTrips }.toSet()

/*
 * Task #3. Find all the passengers, who were taken by a given driver more than once.
 */
fun TaxiPark.findFrequentPassengers(driver: Driver): Set<Passenger> {
    val listOfPassengersFromDesignatedDriver = trips.groupBy { it.driver }.filter { it.component1() == driver }.flatMap { it.component2() }.flatMap { it.passengers }
    return listOfPassengersFromDesignatedDriver.filter { p -> listOfPassengersFromDesignatedDriver.count { p == it } > 1 }.toSet()
}

/*
 * Task #4. Find the passengers who had a discount for majority of their trips.
 */
fun TaxiPark.findSmartPassengers(): Set<Passenger> =
    if (trips.isNullOrEmpty()) emptySet() else
        allPassengers.filter { p -> trips.filter {it.passengers.contains(p)}.map { if (it.discount == null) -1 else 1}.sum() > 0 }.toSet()

/*
 * Task #5. Find the most frequent trip duration among minute periods 0..9, 10..19, 20..29, and so on.
 * Return any period if many are the most frequent, return `null` if there're no trips.
 */
fun TaxiPark.findTheMostFrequentTripDurationPeriod(): IntRange? {
    if (trips.isNullOrEmpty()) return null

    val maxDuration = 0..(trips.map { it.duration }.max() ?: 0).div(10)
    val durationRanges = maxDuration.map { (it * 10)..((it * 10) + 9) }
    val durationCount = durationRanges zip durationRanges.map { r -> trips.count { it.duration in r } }

    return durationCount.maxBy { it.second }?.first
}

/*
 * Task #6.
 * Check whether 20% of the drivers contribute 80% of the income.
 */
fun TaxiPark.checkParetoPrinciple(): Boolean {
    val orderedIncomeByDriver = allDrivers.map { d -> trips.filter { it.driver == d }.sumByDouble { it.cost } }.sorted()
    println(orderedIncomeByDriver)
    val (income20, income80) = orderedIncomeByDriver.chunked(Math.ceil(orderedIncomeByDriver.size * .8).toInt())

    return income20.sum() > income80.sum()
}