package kotlin.exercises

fun main(args: Array<String>) {
    val s = "kotlin"
    println(s as? Int)    // null
    println(s as Int?)    // exception
}