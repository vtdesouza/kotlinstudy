package kotlin.exercises

import java.lang.IllegalArgumentException
import java.lang.IndexOutOfBoundsException
import java.math.BigInteger

class Rational(number:String) : Comparable<Rational> {

    var numerator = BigInteger.ZERO
    var denominator = BigInteger.ONE

    init {
        try {
            val r = number.split("/").map { it.toBigInteger() }
            if ((r.size > 2)) throw IllegalArgumentException("A rational number must be formed by a [/] between two Integers or only a Integer value.")


            this.numerator = if (number.count { it == '-'} == 1) r[0].abs().negate() else r[0].abs()
            this.denominator = r[1].abs()

        } catch (e: NumberFormatException) {
            throw IllegalArgumentException("A rational number must be formed by a [/] between two Integers or only a Integer value.", e)
        } catch (e: IndexOutOfBoundsException) {
            /* Do nothing */
        }
    }

    constructor(number: Int) : this(number.toString())

    override fun toString(): String {
        val simple = simplify()
        return "${simple.numerator}" + (if (simple.denominator != BigInteger.ONE) "/${simple.denominator}" else "")

//        TODO entender pq essa implementação gera um StackOverFlow
//        val simple = simplify()
//        return "" + simple.numerator + (if (simple.denominator != BigInteger.ONE) "/$simple.denominator" else "")
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true

        val simpleOther = if (other is String) other.toRational() else (other as Rational).simplify()
        val simpleThis = this.simplify()


        return (simpleThis.denominator == simpleOther.denominator) && (simpleThis.numerator == simpleOther.numerator)
    }

    fun simplify(): Rational {
        val gcd = numerator.gcd(denominator)
        return Rational("${numerator.div(gcd)}/${denominator.div(gcd)}")
    }

    override fun compareTo(other: Rational): Int {
        val d = this.denominator * other.denominator
        return ((d/this.denominator) * this.numerator).compareTo((d/other.denominator) * other.numerator)
    }
}

infix fun Int.divBy(other: Int) : Rational = Rational("$this/$other")

infix fun Long.divBy(other: Long) : Rational = this.toBigInteger() divBy other.toBigInteger()

infix fun BigInteger.divBy(other: BigInteger) : Rational = Rational("${this}/${other}")

operator fun Rational.plus(other: Rational) : Rational {
    val d = this.denominator * other.denominator
    val numeratorSum = ((d/this.denominator) * this.numerator) + ((d/other.denominator) * other.numerator)
    return Rational("$numeratorSum/$d").simplify()
}

operator fun Rational.minus(other: Rational) : Rational {
    val d = this.denominator * other.denominator
    val numeratorMinus = ((d/this.denominator) * this.numerator) - ((d/other.denominator) * other.numerator)
    return Rational("$numeratorMinus/$d").simplify()
}

operator fun Rational.times(other: Rational): Rational =
    Rational("${this.numerator * other.numerator}/${this.denominator * other.denominator}").simplify()

operator fun Rational.div(other: Rational): Rational =
    Rational("${this.numerator * other.denominator}/${this.denominator * other.numerator}").simplify()

operator fun Rational.unaryMinus(): Rational =
    this.minus(1.toRational()).simplify()

fun Int.toRational(): Rational = Rational("$this").simplify()
fun String.toRational(): Rational = Rational(this).simplify()

fun main() {
    val half = 1 divBy 2
    val third = 1 divBy 3

    val sum: Rational = half + third
    println(5 divBy 6 == sum)

    val difference: Rational = half - third
    println(1 divBy 6 == difference)

    val product: Rational = half * third
    println(1 divBy 6 == product)

    val quotient: Rational = half / third
    println(3 divBy 2 == quotient)

    val negation: Rational = -half
    println(-1 divBy 2 == negation)

    println((2 divBy 1).toString() == "2")
    println((-2 divBy 4).toString() == "-1/2")
    println("117/1098".toRational().toString() == "13/122")

    val twoThirds = 2 divBy 3
    println(half < twoThirds)

    println(half in third..twoThirds)
    println((95 divBy  8) in (516 divBy 46)..(1101 divBy 92))

    println(2000000000L divBy 4000000000L == 1 divBy 2)

    println("912016490186296920119201192141970416029".toBigInteger() divBy
            "1824032980372593840238402384283940832058".toBigInteger() == 1 divBy 2)
}