package kotlin.exercises

fun String.isNice(): Boolean {
    val results = listOf<Boolean>(condition1(this), condition2(this), condition3(this))

    return results.filterNot {it}.size <= 1
}

val condition1: (String) -> Boolean = { !(it.contains("bu")
        || it.contains("ba")
        || it.contains("be"))}

val condition2: (String) -> Boolean = { s -> s.count { "aeiou".contains(it) } >= 3 }

val condition3: (String) -> Boolean = { s -> s.any { s.contains("$it$it") }  }