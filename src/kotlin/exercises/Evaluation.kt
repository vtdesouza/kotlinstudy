package kotlin.exercises

data class Evaluation(val rightPosition: Int, val wrongPosition: Int)

fun evaluateGuess(secret: String, guess: String): Evaluation {

    val rightPositions = secret.zip(guess).count { it.first == it.second }
//    Equivalent expressions
//    val rightPositions = secret.zip(guess).count { a ->  a.first == a.second }
//    val rightPositions = secret.zip(guess).count { (a, b) -> a == b }

    val commonLetters = "ABCDEF".sumBy { ch ->
        Math.min(secret.count { ch == it }, guess.count { ch == it })
    }
    return Evaluation(rightPositions, commonLetters - rightPositions)
//    return Evaluation(rightPositions, 9)
}

fun main() {
    var e:Evaluation

    e = evaluateGuess("AAAA", "AAAA")
    println("AAAA|AAAA = ${e.rightPosition}, ${e.wrongPosition} [4, 0]")

    e = evaluateGuess("ABCD", "ABCD")
    println("ABCD|ABCD = ${e.rightPosition}, ${e.wrongPosition} [4, 0]")

    e = evaluateGuess("ABCD", "CDBA")
    println("ABCD|CDBA = ${e.rightPosition}, ${e.wrongPosition} [0, 4]")

    e = evaluateGuess("ABCD", "ABDC")
    println("ABCD|ABDC = ${e.rightPosition}, ${e.wrongPosition} [2, 2]")

    e = evaluateGuess("AABC", "ADFE")
    println("AABC|ADFE = ${e.rightPosition}, ${e.wrongPosition} [1, 0]")

    e = evaluateGuess("AABC", "DEAA")
    println("AABC|DEAA = ${e.rightPosition}, ${e.wrongPosition} [0, 2]")

    e = evaluateGuess("ADFE", "AABC")
    println("ADFE|AABC = ${e.rightPosition}, ${e.wrongPosition} [1, 0]")

    e = evaluateGuess("DCBA", "ABCD")
    println("DCBA|ABCD = ${e.rightPosition}, ${e.wrongPosition} [0, 4]")

    e = evaluateGuess("ABCD", "EAAA")
    println("ABCD|EAAA = ${e.rightPosition}, ${e.wrongPosition} [0, 1]")

    e = evaluateGuess("AABC", "ADFE")
    println("AABC|ADFE = ${e.rightPosition}, ${e.wrongPosition} [1, 0]")
}
