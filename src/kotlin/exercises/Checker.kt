package kotlin.exercises


// Implement the function that checks whether a string is a valid identifier.
// A valid identifier is a non-empty string that starts with a letter or underscore
// and consists of only letters, digits and underscores.
//fun isValidIdentifier(s: String): Boolean {
//
//    return if (s.isNullOrEmpty() || !(s.first().isLetter() || s.first() == '_')) false
//        else s.substring(1).all { it.isLetterOrDigit() || it == '_' }
//}

fun isValidIdentifier(s: String) = if (s.isNullOrEmpty() || !(s.first().isLetter() || s.first() == '_')) false
    else s.substring(1).all { it.isLetterOrDigit() || it == '_' }

fun main(args: Array<String>) {
    println("T : ${isValidIdentifier("name")}")   // true
    println("T : ${isValidIdentifier("_name")}")  // true
    println("T : ${isValidIdentifier("_12")}")    // true
    println("F : ${isValidIdentifier("")}")       // false
    println("F : ${isValidIdentifier("012")}")    // false
    println("F : ${isValidIdentifier("no$")}")    // false
}